﻿namespace AMM_Reporting
{
    partial class frmAMM_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCntrlMain = new System.Windows.Forms.TabControl();
            this.tabPageElectron = new System.Windows.Forms.TabPage();
            this.lbElectronEndDate = new System.Windows.Forms.Label();
            this.dtpElectronEndDate = new System.Windows.Forms.DateTimePicker();
            this.lbElectronBeginDate = new System.Windows.Forms.Label();
            this.dtpElectronBeginDate = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.rbElectronByPartNum = new System.Windows.Forms.RadioButton();
            this.rbElectronByDate = new System.Windows.Forms.RadioButton();
            this.crvElectronRpt = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnEP_Exit = new System.Windows.Forms.Button();
            this.btnEP_DisplayRpt = new System.Windows.Forms.Button();
            this.tabPageOAU_PurParts = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpOAU_PartsEndDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpOAU_PartsBeginDate = new System.Windows.Forms.DateTimePicker();
            this.btnOAUPartsExit = new System.Windows.Forms.Button();
            this.crvOAUPartsRpt = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnOAUPartsRptDisplay = new System.Windows.Forms.Button();
            this.tabPageIndPart = new System.Windows.Forms.TabPage();
            this.cbPartNumber = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpIndPartEndDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpIndPartBeginDate = new System.Windows.Forms.DateTimePicker();
            this.btnOAU_IndividualPartExit = new System.Windows.Forms.Button();
            this.btnOAU_IndividualPartDisplay = new System.Windows.Forms.Button();
            this.crvOAU_IndPartTranRpt = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tabPageByUser = new System.Windows.Forms.TabPage();
            this.cbUserID = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpIndUserEndDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpIndUserBeginDate = new System.Windows.Forms.DateTimePicker();
            this.btnIndUserExit = new System.Windows.Forms.Button();
            this.btnIndUserDisplayRpt = new System.Windows.Forms.Button();
            this.crvOAU_IndUserTranRpt = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tabPageMetalTran = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpOAU_MetalTranEndDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpOAU_MetalTranBeginDate = new System.Windows.Forms.DateTimePicker();
            this.btnOAU_MetalTranExit = new System.Windows.Forms.Button();
            this.crvOAU_MetalRpt = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnOAU_MetalTranDisplay = new System.Windows.Forms.Button();
            this.tabPagePullListAudit = new System.Windows.Forms.TabPage();
            this.rbPickListAuditAllJobs = new System.Windows.Forms.RadioButton();
            this.rbPickListAuditClosedJobs = new System.Windows.Forms.RadioButton();
            this.rbPickListAuditOpenJobs = new System.Windows.Forms.RadioButton();
            this.btnPickListAuditReset = new System.Windows.Forms.Button();
            this.dtpPickListDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.btnPickListAuditDisplay = new System.Windows.Forms.Button();
            this.btnPickListAuditExit = new System.Windows.Forms.Button();
            this.dgvPickListMain = new System.Windows.Forms.DataGridView();
            this.tabPageAdjQtyAudit = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpAdjQtyEndDate = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.dtpAdjQtyStartDate = new System.Windows.Forms.DateTimePicker();
            this.btnAdjQtyExit = new System.Windows.Forms.Button();
            this.btnAdjQtyDisplay = new System.Windows.Forms.Button();
            this.crvAdjQtyAuditRpt = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tabPageNegQtyAudit = new System.Windows.Forms.TabPage();
            this.btnNegQtyExit = new System.Windows.Forms.Button();
            this.btnNegQtyDisplay = new System.Windows.Forms.Button();
            this.crvNegQtyRpt = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tabPageReportBin = new System.Windows.Forms.TabPage();
            this.cb_RB_L5REC = new System.Windows.Forms.CheckBox();
            this.cb_RB_L4REC = new System.Windows.Forms.CheckBox();
            this.cb_RB_LBIN1 = new System.Windows.Forms.CheckBox();
            this.cb_RB_L1REC = new System.Windows.Forms.CheckBox();
            this.cb_RB_ELVMI = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cb_RB_LBIN2 = new System.Windows.Forms.CheckBox();
            this.cb_RB_L2MF = new System.Windows.Forms.CheckBox();
            this.cb_RB_L2EL = new System.Windows.Forms.CheckBox();
            this.cb_RB_L2AS = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.crv_BinReport = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btn_RB_Exit = new System.Windows.Forms.Button();
            this.btn_RB_DisplayRpt = new System.Windows.Forms.Button();
            this.tabCntrlMain.SuspendLayout();
            this.tabPageElectron.SuspendLayout();
            this.tabPageOAU_PurParts.SuspendLayout();
            this.tabPageIndPart.SuspendLayout();
            this.tabPageByUser.SuspendLayout();
            this.tabPageMetalTran.SuspendLayout();
            this.tabPagePullListAudit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickListMain)).BeginInit();
            this.tabPageAdjQtyAudit.SuspendLayout();
            this.tabPageNegQtyAudit.SuspendLayout();
            this.tabPageReportBin.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCntrlMain
            // 
            this.tabCntrlMain.Controls.Add(this.tabPageElectron);
            this.tabCntrlMain.Controls.Add(this.tabPageOAU_PurParts);
            this.tabCntrlMain.Controls.Add(this.tabPageIndPart);
            this.tabCntrlMain.Controls.Add(this.tabPageByUser);
            this.tabCntrlMain.Controls.Add(this.tabPageMetalTran);
            this.tabCntrlMain.Controls.Add(this.tabPagePullListAudit);
            this.tabCntrlMain.Controls.Add(this.tabPageAdjQtyAudit);
            this.tabCntrlMain.Controls.Add(this.tabPageNegQtyAudit);
            this.tabCntrlMain.Controls.Add(this.tabPageReportBin);
            this.tabCntrlMain.Location = new System.Drawing.Point(24, 10);
            this.tabCntrlMain.Name = "tabCntrlMain";
            this.tabCntrlMain.SelectedIndex = 0;
            this.tabCntrlMain.Size = new System.Drawing.Size(1355, 975);
            this.tabCntrlMain.TabIndex = 0;
            this.tabCntrlMain.SelectedIndexChanged += new System.EventHandler(this.tabCntrlMain_SelectedIndexChanged);
            // 
            // tabPageElectron
            // 
            this.tabPageElectron.Controls.Add(this.lbElectronEndDate);
            this.tabPageElectron.Controls.Add(this.dtpElectronEndDate);
            this.tabPageElectron.Controls.Add(this.lbElectronBeginDate);
            this.tabPageElectron.Controls.Add(this.dtpElectronBeginDate);
            this.tabPageElectron.Controls.Add(this.label11);
            this.tabPageElectron.Controls.Add(this.rbElectronByPartNum);
            this.tabPageElectron.Controls.Add(this.rbElectronByDate);
            this.tabPageElectron.Controls.Add(this.crvElectronRpt);
            this.tabPageElectron.Controls.Add(this.btnEP_Exit);
            this.tabPageElectron.Controls.Add(this.btnEP_DisplayRpt);
            this.tabPageElectron.Location = new System.Drawing.Point(4, 22);
            this.tabPageElectron.Name = "tabPageElectron";
            this.tabPageElectron.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageElectron.Size = new System.Drawing.Size(1347, 949);
            this.tabPageElectron.TabIndex = 0;
            this.tabPageElectron.Text = "Electron Parts";
            this.tabPageElectron.UseVisualStyleBackColor = true;
            // 
            // lbElectronEndDate
            // 
            this.lbElectronEndDate.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbElectronEndDate.Location = new System.Drawing.Point(594, 9);
            this.lbElectronEndDate.Name = "lbElectronEndDate";
            this.lbElectronEndDate.Size = new System.Drawing.Size(100, 20);
            this.lbElectronEndDate.TabIndex = 14;
            this.lbElectronEndDate.Text = "End Date:";
            this.lbElectronEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpElectronEndDate
            // 
            this.dtpElectronEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpElectronEndDate.Location = new System.Drawing.Point(700, 11);
            this.dtpElectronEndDate.Name = "dtpElectronEndDate";
            this.dtpElectronEndDate.Size = new System.Drawing.Size(104, 20);
            this.dtpElectronEndDate.TabIndex = 13;
            // 
            // lbElectronBeginDate
            // 
            this.lbElectronBeginDate.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbElectronBeginDate.Location = new System.Drawing.Point(379, 9);
            this.lbElectronBeginDate.Name = "lbElectronBeginDate";
            this.lbElectronBeginDate.Size = new System.Drawing.Size(100, 20);
            this.lbElectronBeginDate.TabIndex = 12;
            this.lbElectronBeginDate.Text = "Begin Date:";
            this.lbElectronBeginDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpElectronBeginDate
            // 
            this.dtpElectronBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpElectronBeginDate.Location = new System.Drawing.Point(485, 11);
            this.dtpElectronBeginDate.Name = "dtpElectronBeginDate";
            this.dtpElectronBeginDate.Size = new System.Drawing.Size(104, 20);
            this.dtpElectronBeginDate.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Display Report by:";
            // 
            // rbElectronByPartNum
            // 
            this.rbElectronByPartNum.AutoSize = true;
            this.rbElectronByPartNum.Location = new System.Drawing.Point(222, 11);
            this.rbElectronByPartNum.Name = "rbElectronByPartNum";
            this.rbElectronByPartNum.Size = new System.Drawing.Size(84, 17);
            this.rbElectronByPartNum.TabIndex = 5;
            this.rbElectronByPartNum.Text = "Part Number";
            this.rbElectronByPartNum.UseVisualStyleBackColor = true;
            this.rbElectronByPartNum.CheckedChanged += new System.EventHandler(this.rbElectronByPartNum_CheckedChanged);
            // 
            // rbElectronByDate
            // 
            this.rbElectronByDate.AutoSize = true;
            this.rbElectronByDate.Checked = true;
            this.rbElectronByDate.Location = new System.Drawing.Point(147, 11);
            this.rbElectronByDate.Name = "rbElectronByDate";
            this.rbElectronByDate.Size = new System.Drawing.Size(48, 17);
            this.rbElectronByDate.TabIndex = 4;
            this.rbElectronByDate.TabStop = true;
            this.rbElectronByDate.Text = "Date";
            this.rbElectronByDate.UseVisualStyleBackColor = true;
            this.rbElectronByDate.CheckedChanged += new System.EventHandler(this.rbElectronByDate_CheckedChanged);
            // 
            // crvElectronRpt
            // 
            this.crvElectronRpt.ActiveViewIndex = -1;
            this.crvElectronRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvElectronRpt.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvElectronRpt.Location = new System.Drawing.Point(6, 37);
            this.crvElectronRpt.Name = "crvElectronRpt";
            this.crvElectronRpt.Size = new System.Drawing.Size(1079, 908);
            this.crvElectronRpt.TabIndex = 3;
            this.crvElectronRpt.Visible = false;
            // 
            // btnEP_Exit
            // 
            this.btnEP_Exit.Location = new System.Drawing.Point(994, 8);
            this.btnEP_Exit.Name = "btnEP_Exit";
            this.btnEP_Exit.Size = new System.Drawing.Size(92, 23);
            this.btnEP_Exit.TabIndex = 2;
            this.btnEP_Exit.Text = "Exit";
            this.btnEP_Exit.UseVisualStyleBackColor = true;
            this.btnEP_Exit.Click += new System.EventHandler(this.btnEP_Exit_Click);
            // 
            // btnEP_DisplayRpt
            // 
            this.btnEP_DisplayRpt.Location = new System.Drawing.Point(889, 8);
            this.btnEP_DisplayRpt.Name = "btnEP_DisplayRpt";
            this.btnEP_DisplayRpt.Size = new System.Drawing.Size(92, 23);
            this.btnEP_DisplayRpt.TabIndex = 1;
            this.btnEP_DisplayRpt.Text = "Display Report";
            this.btnEP_DisplayRpt.UseVisualStyleBackColor = true;
            this.btnEP_DisplayRpt.Click += new System.EventHandler(this.btnEP_DisplayRpt_Click);
            // 
            // tabPageOAU_PurParts
            // 
            this.tabPageOAU_PurParts.Controls.Add(this.label2);
            this.tabPageOAU_PurParts.Controls.Add(this.dtpOAU_PartsEndDate);
            this.tabPageOAU_PurParts.Controls.Add(this.label1);
            this.tabPageOAU_PurParts.Controls.Add(this.dtpOAU_PartsBeginDate);
            this.tabPageOAU_PurParts.Controls.Add(this.btnOAUPartsExit);
            this.tabPageOAU_PurParts.Controls.Add(this.crvOAUPartsRpt);
            this.tabPageOAU_PurParts.Controls.Add(this.btnOAUPartsRptDisplay);
            this.tabPageOAU_PurParts.Location = new System.Drawing.Point(4, 22);
            this.tabPageOAU_PurParts.Name = "tabPageOAU_PurParts";
            this.tabPageOAU_PurParts.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOAU_PurParts.Size = new System.Drawing.Size(1347, 949);
            this.tabPageOAU_PurParts.TabIndex = 1;
            this.tabPageOAU_PurParts.Text = "OAU Purchase Parts";
            this.tabPageOAU_PurParts.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(653, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "End Date:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpOAU_PartsEndDate
            // 
            this.dtpOAU_PartsEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOAU_PartsEndDate.Location = new System.Drawing.Point(759, 11);
            this.dtpOAU_PartsEndDate.Name = "dtpOAU_PartsEndDate";
            this.dtpOAU_PartsEndDate.Size = new System.Drawing.Size(104, 20);
            this.dtpOAU_PartsEndDate.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(438, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Begin Date:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpOAU_PartsBeginDate
            // 
            this.dtpOAU_PartsBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOAU_PartsBeginDate.Location = new System.Drawing.Point(544, 11);
            this.dtpOAU_PartsBeginDate.Name = "dtpOAU_PartsBeginDate";
            this.dtpOAU_PartsBeginDate.Size = new System.Drawing.Size(104, 20);
            this.dtpOAU_PartsBeginDate.TabIndex = 7;
            // 
            // btnOAUPartsExit
            // 
            this.btnOAUPartsExit.Location = new System.Drawing.Point(1214, 5);
            this.btnOAUPartsExit.Name = "btnOAUPartsExit";
            this.btnOAUPartsExit.Size = new System.Drawing.Size(92, 23);
            this.btnOAUPartsExit.TabIndex = 6;
            this.btnOAUPartsExit.Text = "Exit";
            this.btnOAUPartsExit.UseVisualStyleBackColor = true;
            this.btnOAUPartsExit.Click += new System.EventHandler(this.btnOAUPartsExit_Click);
            // 
            // crvOAUPartsRpt
            // 
            this.crvOAUPartsRpt.ActiveViewIndex = -1;
            this.crvOAUPartsRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvOAUPartsRpt.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvOAUPartsRpt.Location = new System.Drawing.Point(6, 37);
            this.crvOAUPartsRpt.Name = "crvOAUPartsRpt";
            this.crvOAUPartsRpt.Size = new System.Drawing.Size(1300, 880);
            this.crvOAUPartsRpt.TabIndex = 5;
            this.crvOAUPartsRpt.Visible = false;
            // 
            // btnOAUPartsRptDisplay
            // 
            this.btnOAUPartsRptDisplay.Location = new System.Drawing.Point(1109, 5);
            this.btnOAUPartsRptDisplay.Name = "btnOAUPartsRptDisplay";
            this.btnOAUPartsRptDisplay.Size = new System.Drawing.Size(92, 23);
            this.btnOAUPartsRptDisplay.TabIndex = 3;
            this.btnOAUPartsRptDisplay.Text = "Display Report";
            this.btnOAUPartsRptDisplay.UseVisualStyleBackColor = true;
            this.btnOAUPartsRptDisplay.Click += new System.EventHandler(this.btnOAUPartsDisplay_Click);
            // 
            // tabPageIndPart
            // 
            this.tabPageIndPart.Controls.Add(this.cbPartNumber);
            this.tabPageIndPart.Controls.Add(this.label5);
            this.tabPageIndPart.Controls.Add(this.label3);
            this.tabPageIndPart.Controls.Add(this.dtpIndPartEndDate);
            this.tabPageIndPart.Controls.Add(this.label4);
            this.tabPageIndPart.Controls.Add(this.dtpIndPartBeginDate);
            this.tabPageIndPart.Controls.Add(this.btnOAU_IndividualPartExit);
            this.tabPageIndPart.Controls.Add(this.btnOAU_IndividualPartDisplay);
            this.tabPageIndPart.Controls.Add(this.crvOAU_IndPartTranRpt);
            this.tabPageIndPart.Location = new System.Drawing.Point(4, 22);
            this.tabPageIndPart.Name = "tabPageIndPart";
            this.tabPageIndPart.Size = new System.Drawing.Size(1347, 949);
            this.tabPageIndPart.TabIndex = 2;
            this.tabPageIndPart.Text = "Individual Part";
            this.tabPageIndPart.UseVisualStyleBackColor = true;
            // 
            // cbPartNumber
            // 
            this.cbPartNumber.FormattingEnabled = true;
            this.cbPartNumber.Location = new System.Drawing.Point(342, 6);
            this.cbPartNumber.Name = "cbPartNumber";
            this.cbPartNumber.Size = new System.Drawing.Size(252, 21);
            this.cbPartNumber.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(224, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "Part Number:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(815, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "End Date:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpIndPartEndDate
            // 
            this.dtpIndPartEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpIndPartEndDate.Location = new System.Drawing.Point(921, 10);
            this.dtpIndPartEndDate.Name = "dtpIndPartEndDate";
            this.dtpIndPartEndDate.Size = new System.Drawing.Size(104, 20);
            this.dtpIndPartEndDate.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(600, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "Begin Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpIndPartBeginDate
            // 
            this.dtpIndPartBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpIndPartBeginDate.Location = new System.Drawing.Point(706, 9);
            this.dtpIndPartBeginDate.Name = "dtpIndPartBeginDate";
            this.dtpIndPartBeginDate.Size = new System.Drawing.Size(104, 20);
            this.dtpIndPartBeginDate.TabIndex = 13;
            // 
            // btnOAU_IndividualPartExit
            // 
            this.btnOAU_IndividualPartExit.Location = new System.Drawing.Point(1222, 8);
            this.btnOAU_IndividualPartExit.Name = "btnOAU_IndividualPartExit";
            this.btnOAU_IndividualPartExit.Size = new System.Drawing.Size(92, 23);
            this.btnOAU_IndividualPartExit.TabIndex = 12;
            this.btnOAU_IndividualPartExit.Text = "Exit";
            this.btnOAU_IndividualPartExit.UseVisualStyleBackColor = true;
            this.btnOAU_IndividualPartExit.Click += new System.EventHandler(this.btnOAU_IndividualPartExit_Click);
            // 
            // btnOAU_IndividualPartDisplay
            // 
            this.btnOAU_IndividualPartDisplay.Location = new System.Drawing.Point(1117, 8);
            this.btnOAU_IndividualPartDisplay.Name = "btnOAU_IndividualPartDisplay";
            this.btnOAU_IndividualPartDisplay.Size = new System.Drawing.Size(92, 23);
            this.btnOAU_IndividualPartDisplay.TabIndex = 11;
            this.btnOAU_IndividualPartDisplay.Text = "Display Report";
            this.btnOAU_IndividualPartDisplay.UseVisualStyleBackColor = true;
            this.btnOAU_IndividualPartDisplay.Click += new System.EventHandler(this.btnOAU_IndividualPartDisplay_Click);
            // 
            // crvOAU_IndPartTranRpt
            // 
            this.crvOAU_IndPartTranRpt.ActiveViewIndex = -1;
            this.crvOAU_IndPartTranRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvOAU_IndPartTranRpt.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvOAU_IndPartTranRpt.Location = new System.Drawing.Point(12, 38);
            this.crvOAU_IndPartTranRpt.Name = "crvOAU_IndPartTranRpt";
            this.crvOAU_IndPartTranRpt.Size = new System.Drawing.Size(1300, 880);
            this.crvOAU_IndPartTranRpt.TabIndex = 6;
            this.crvOAU_IndPartTranRpt.Visible = false;
            // 
            // tabPageByUser
            // 
            this.tabPageByUser.Controls.Add(this.cbUserID);
            this.tabPageByUser.Controls.Add(this.label6);
            this.tabPageByUser.Controls.Add(this.label7);
            this.tabPageByUser.Controls.Add(this.dtpIndUserEndDate);
            this.tabPageByUser.Controls.Add(this.label8);
            this.tabPageByUser.Controls.Add(this.dtpIndUserBeginDate);
            this.tabPageByUser.Controls.Add(this.btnIndUserExit);
            this.tabPageByUser.Controls.Add(this.btnIndUserDisplayRpt);
            this.tabPageByUser.Controls.Add(this.crvOAU_IndUserTranRpt);
            this.tabPageByUser.Location = new System.Drawing.Point(4, 22);
            this.tabPageByUser.Name = "tabPageByUser";
            this.tabPageByUser.Size = new System.Drawing.Size(1347, 949);
            this.tabPageByUser.TabIndex = 3;
            this.tabPageByUser.Text = "Individual User";
            this.tabPageByUser.UseVisualStyleBackColor = true;
            // 
            // cbUserID
            // 
            this.cbUserID.FormattingEnabled = true;
            this.cbUserID.Location = new System.Drawing.Point(342, 9);
            this.cbUserID.Name = "cbUserID";
            this.cbUserID.Size = new System.Drawing.Size(252, 21);
            this.cbUserID.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(224, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 20);
            this.label6.TabIndex = 25;
            this.label6.Text = "User Name:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(815, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 20);
            this.label7.TabIndex = 24;
            this.label7.Text = "End Date:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpIndUserEndDate
            // 
            this.dtpIndUserEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpIndUserEndDate.Location = new System.Drawing.Point(921, 10);
            this.dtpIndUserEndDate.Name = "dtpIndUserEndDate";
            this.dtpIndUserEndDate.Size = new System.Drawing.Size(104, 20);
            this.dtpIndUserEndDate.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(600, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 20);
            this.label8.TabIndex = 22;
            this.label8.Text = "Begin Date:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpIndUserBeginDate
            // 
            this.dtpIndUserBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpIndUserBeginDate.Location = new System.Drawing.Point(706, 9);
            this.dtpIndUserBeginDate.Name = "dtpIndUserBeginDate";
            this.dtpIndUserBeginDate.Size = new System.Drawing.Size(104, 20);
            this.dtpIndUserBeginDate.TabIndex = 21;
            // 
            // btnIndUserExit
            // 
            this.btnIndUserExit.Location = new System.Drawing.Point(1222, 8);
            this.btnIndUserExit.Name = "btnIndUserExit";
            this.btnIndUserExit.Size = new System.Drawing.Size(92, 23);
            this.btnIndUserExit.TabIndex = 20;
            this.btnIndUserExit.Text = "Exit";
            this.btnIndUserExit.UseVisualStyleBackColor = true;
            this.btnIndUserExit.Click += new System.EventHandler(this.btnIndUserExit_Click);
            // 
            // btnIndUserDisplayRpt
            // 
            this.btnIndUserDisplayRpt.Location = new System.Drawing.Point(1117, 8);
            this.btnIndUserDisplayRpt.Name = "btnIndUserDisplayRpt";
            this.btnIndUserDisplayRpt.Size = new System.Drawing.Size(92, 23);
            this.btnIndUserDisplayRpt.TabIndex = 19;
            this.btnIndUserDisplayRpt.Text = "Display Report";
            this.btnIndUserDisplayRpt.UseVisualStyleBackColor = true;
            this.btnIndUserDisplayRpt.Click += new System.EventHandler(this.btnIndUserDisplayRpt_Click);
            // 
            // crvOAU_IndUserTranRpt
            // 
            this.crvOAU_IndUserTranRpt.ActiveViewIndex = -1;
            this.crvOAU_IndUserTranRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvOAU_IndUserTranRpt.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvOAU_IndUserTranRpt.Location = new System.Drawing.Point(15, 38);
            this.crvOAU_IndUserTranRpt.Name = "crvOAU_IndUserTranRpt";
            this.crvOAU_IndUserTranRpt.Size = new System.Drawing.Size(1300, 880);
            this.crvOAU_IndUserTranRpt.TabIndex = 7;
            this.crvOAU_IndUserTranRpt.Visible = false;           
            // 
            // tabPageMetalTran
            // 
            this.tabPageMetalTran.Controls.Add(this.label9);
            this.tabPageMetalTran.Controls.Add(this.dtpOAU_MetalTranEndDate);
            this.tabPageMetalTran.Controls.Add(this.label10);
            this.tabPageMetalTran.Controls.Add(this.dtpOAU_MetalTranBeginDate);
            this.tabPageMetalTran.Controls.Add(this.btnOAU_MetalTranExit);
            this.tabPageMetalTran.Controls.Add(this.crvOAU_MetalRpt);
            this.tabPageMetalTran.Controls.Add(this.btnOAU_MetalTranDisplay);
            this.tabPageMetalTran.Location = new System.Drawing.Point(4, 22);
            this.tabPageMetalTran.Name = "tabPageMetalTran";
            this.tabPageMetalTran.Size = new System.Drawing.Size(1347, 949);
            this.tabPageMetalTran.TabIndex = 4;
            this.tabPageMetalTran.Text = "Metal Trans";
            this.tabPageMetalTran.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(653, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "End Date:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpOAU_MetalTranEndDate
            // 
            this.dtpOAU_MetalTranEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOAU_MetalTranEndDate.Location = new System.Drawing.Point(759, 11);
            this.dtpOAU_MetalTranEndDate.Name = "dtpOAU_MetalTranEndDate";
            this.dtpOAU_MetalTranEndDate.Size = new System.Drawing.Size(104, 20);
            this.dtpOAU_MetalTranEndDate.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(438, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 20);
            this.label10.TabIndex = 15;
            this.label10.Text = "Begin Date:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpOAU_MetalTranBeginDate
            // 
            this.dtpOAU_MetalTranBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpOAU_MetalTranBeginDate.Location = new System.Drawing.Point(544, 11);
            this.dtpOAU_MetalTranBeginDate.Name = "dtpOAU_MetalTranBeginDate";
            this.dtpOAU_MetalTranBeginDate.Size = new System.Drawing.Size(104, 20);
            this.dtpOAU_MetalTranBeginDate.TabIndex = 14;
            // 
            // btnOAU_MetalTranExit
            // 
            this.btnOAU_MetalTranExit.Location = new System.Drawing.Point(1214, 5);
            this.btnOAU_MetalTranExit.Name = "btnOAU_MetalTranExit";
            this.btnOAU_MetalTranExit.Size = new System.Drawing.Size(92, 23);
            this.btnOAU_MetalTranExit.TabIndex = 13;
            this.btnOAU_MetalTranExit.Text = "Exit";
            this.btnOAU_MetalTranExit.UseVisualStyleBackColor = true;
            this.btnOAU_MetalTranExit.Click += new System.EventHandler(this.btnOAU_MetalTranExit_Click);
            // 
            // crvOAU_MetalRpt
            // 
            this.crvOAU_MetalRpt.ActiveViewIndex = -1;
            this.crvOAU_MetalRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvOAU_MetalRpt.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvOAU_MetalRpt.Location = new System.Drawing.Point(6, 37);
            this.crvOAU_MetalRpt.Name = "crvOAU_MetalRpt";
            this.crvOAU_MetalRpt.Size = new System.Drawing.Size(1300, 880);
            this.crvOAU_MetalRpt.TabIndex = 12;
            this.crvOAU_MetalRpt.Visible = false;
            // 
            // btnOAU_MetalTranDisplay
            // 
            this.btnOAU_MetalTranDisplay.Location = new System.Drawing.Point(1109, 5);
            this.btnOAU_MetalTranDisplay.Name = "btnOAU_MetalTranDisplay";
            this.btnOAU_MetalTranDisplay.Size = new System.Drawing.Size(92, 23);
            this.btnOAU_MetalTranDisplay.TabIndex = 11;
            this.btnOAU_MetalTranDisplay.Text = "Display Report";
            this.btnOAU_MetalTranDisplay.UseVisualStyleBackColor = true;
            this.btnOAU_MetalTranDisplay.Click += new System.EventHandler(this.btnOAU_MetalTranDisplay_Click);
            // 
            // tabPagePullListAudit
            // 
            this.tabPagePullListAudit.Controls.Add(this.rbPickListAuditAllJobs);
            this.tabPagePullListAudit.Controls.Add(this.rbPickListAuditClosedJobs);
            this.tabPagePullListAudit.Controls.Add(this.rbPickListAuditOpenJobs);
            this.tabPagePullListAudit.Controls.Add(this.btnPickListAuditReset);
            this.tabPagePullListAudit.Controls.Add(this.dtpPickListDate);
            this.tabPagePullListAudit.Controls.Add(this.label13);
            this.tabPagePullListAudit.Controls.Add(this.btnPickListAuditDisplay);
            this.tabPagePullListAudit.Controls.Add(this.btnPickListAuditExit);
            this.tabPagePullListAudit.Controls.Add(this.dgvPickListMain);
            this.tabPagePullListAudit.Location = new System.Drawing.Point(4, 22);
            this.tabPagePullListAudit.Name = "tabPagePullListAudit";
            this.tabPagePullListAudit.Size = new System.Drawing.Size(1347, 949);
            this.tabPagePullListAudit.TabIndex = 5;
            this.tabPagePullListAudit.Text = "PullListAudit";
            this.tabPagePullListAudit.UseVisualStyleBackColor = true;
            // 
            // rbPickListAuditAllJobs
            // 
            this.rbPickListAuditAllJobs.AutoSize = true;
            this.rbPickListAuditAllJobs.Location = new System.Drawing.Point(292, 15);
            this.rbPickListAuditAllJobs.Name = "rbPickListAuditAllJobs";
            this.rbPickListAuditAllJobs.Size = new System.Drawing.Size(61, 17);
            this.rbPickListAuditAllJobs.TabIndex = 21;
            this.rbPickListAuditAllJobs.Text = "All Jobs";
            this.rbPickListAuditAllJobs.UseVisualStyleBackColor = true;
            this.rbPickListAuditAllJobs.CheckedChanged += new System.EventHandler(this.rbPickListAuditAllJobs_CheckedChanged);
            // 
            // rbPickListAuditClosedJobs
            // 
            this.rbPickListAuditClosedJobs.AutoSize = true;
            this.rbPickListAuditClosedJobs.Location = new System.Drawing.Point(160, 15);
            this.rbPickListAuditClosedJobs.Name = "rbPickListAuditClosedJobs";
            this.rbPickListAuditClosedJobs.Size = new System.Drawing.Size(82, 17);
            this.rbPickListAuditClosedJobs.TabIndex = 23;
            this.rbPickListAuditClosedJobs.Text = "Closed Jobs";
            this.rbPickListAuditClosedJobs.UseVisualStyleBackColor = true;
            this.rbPickListAuditClosedJobs.CheckedChanged += new System.EventHandler(this.rbPickListAuditClosedJobs_CheckedChanged);
            // 
            // rbPickListAuditOpenJobs
            // 
            this.rbPickListAuditOpenJobs.AutoSize = true;
            this.rbPickListAuditOpenJobs.Checked = true;
            this.rbPickListAuditOpenJobs.ForeColor = System.Drawing.Color.Red;
            this.rbPickListAuditOpenJobs.Location = new System.Drawing.Point(39, 15);
            this.rbPickListAuditOpenJobs.Name = "rbPickListAuditOpenJobs";
            this.rbPickListAuditOpenJobs.Size = new System.Drawing.Size(76, 17);
            this.rbPickListAuditOpenJobs.TabIndex = 22;
            this.rbPickListAuditOpenJobs.TabStop = true;
            this.rbPickListAuditOpenJobs.Text = "Open Jobs";
            this.rbPickListAuditOpenJobs.UseVisualStyleBackColor = true;
            this.rbPickListAuditOpenJobs.CheckedChanged += new System.EventHandler(this.rbPickListAuditOpenJobs_CheckedChanged);
            // 
            // btnPickListAuditReset
            // 
            this.btnPickListAuditReset.ForeColor = System.Drawing.Color.Blue;
            this.btnPickListAuditReset.Location = new System.Drawing.Point(995, 9);
            this.btnPickListAuditReset.Name = "btnPickListAuditReset";
            this.btnPickListAuditReset.Size = new System.Drawing.Size(105, 27);
            this.btnPickListAuditReset.TabIndex = 27;
            this.btnPickListAuditReset.Text = "Reset Screen";
            this.btnPickListAuditReset.UseVisualStyleBackColor = true;
            this.btnPickListAuditReset.Click += new System.EventHandler(this.btnPickListAuditReset_Click);
            // 
            // dtpPickListDate
            // 
            this.dtpPickListDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPickListDate.Location = new System.Drawing.Point(606, 16);
            this.dtpPickListDate.Name = "dtpPickListDate";
            this.dtpPickListDate.Size = new System.Drawing.Size(106, 20);
            this.dtpPickListDate.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(524, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 20);
            this.label13.TabIndex = 25;
            this.label13.Text = "Pick List Date:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPickListAuditDisplay
            // 
            this.btnPickListAuditDisplay.ForeColor = System.Drawing.Color.Blue;
            this.btnPickListAuditDisplay.Location = new System.Drawing.Point(1118, 9);
            this.btnPickListAuditDisplay.Name = "btnPickListAuditDisplay";
            this.btnPickListAuditDisplay.Size = new System.Drawing.Size(105, 27);
            this.btnPickListAuditDisplay.TabIndex = 24;
            this.btnPickListAuditDisplay.Text = "Display Report";
            this.btnPickListAuditDisplay.UseVisualStyleBackColor = true;
            this.btnPickListAuditDisplay.Click += new System.EventHandler(this.btnPickListAuditDisplay_Click);
            // 
            // btnPickListAuditExit
            // 
            this.btnPickListAuditExit.ForeColor = System.Drawing.Color.Red;
            this.btnPickListAuditExit.Location = new System.Drawing.Point(1238, 9);
            this.btnPickListAuditExit.Name = "btnPickListAuditExit";
            this.btnPickListAuditExit.Size = new System.Drawing.Size(105, 27);
            this.btnPickListAuditExit.TabIndex = 19;
            this.btnPickListAuditExit.Text = "Exit";
            this.btnPickListAuditExit.UseVisualStyleBackColor = true;
            this.btnPickListAuditExit.Click += new System.EventHandler(this.buttonMainExit_Click);
            // 
            // dgvPickListMain
            // 
            this.dgvPickListMain.AllowUserToAddRows = false;
            this.dgvPickListMain.AllowUserToDeleteRows = false;
            this.dgvPickListMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPickListMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPickListMain.Location = new System.Drawing.Point(13, 48);
            this.dgvPickListMain.MultiSelect = false;
            this.dgvPickListMain.Name = "dgvPickListMain";
            this.dgvPickListMain.ReadOnly = true;
            this.dgvPickListMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPickListMain.Size = new System.Drawing.Size(1330, 870);
            this.dgvPickListMain.TabIndex = 0;
            // 
            // tabPageAdjQtyAudit
            // 
            this.tabPageAdjQtyAudit.Controls.Add(this.label12);
            this.tabPageAdjQtyAudit.Controls.Add(this.dtpAdjQtyEndDate);
            this.tabPageAdjQtyAudit.Controls.Add(this.label14);
            this.tabPageAdjQtyAudit.Controls.Add(this.dtpAdjQtyStartDate);
            this.tabPageAdjQtyAudit.Controls.Add(this.btnAdjQtyExit);
            this.tabPageAdjQtyAudit.Controls.Add(this.btnAdjQtyDisplay);
            this.tabPageAdjQtyAudit.Controls.Add(this.crvAdjQtyAuditRpt);
            this.tabPageAdjQtyAudit.Location = new System.Drawing.Point(4, 22);
            this.tabPageAdjQtyAudit.Name = "tabPageAdjQtyAudit";
            this.tabPageAdjQtyAudit.Size = new System.Drawing.Size(1347, 949);
            this.tabPageAdjQtyAudit.TabIndex = 6;
            this.tabPageAdjQtyAudit.Text = "AdjQtyAudit";
            this.tabPageAdjQtyAudit.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(646, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 20);
            this.label12.TabIndex = 16;
            this.label12.Text = "End Date:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpAdjQtyEndDate
            // 
            this.dtpAdjQtyEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdjQtyEndDate.Location = new System.Drawing.Point(752, 11);
            this.dtpAdjQtyEndDate.Name = "dtpAdjQtyEndDate";
            this.dtpAdjQtyEndDate.Size = new System.Drawing.Size(104, 20);
            this.dtpAdjQtyEndDate.TabIndex = 15;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(431, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 20);
            this.label14.TabIndex = 14;
            this.label14.Text = "Begin Date:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpAdjQtyStartDate
            // 
            this.dtpAdjQtyStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdjQtyStartDate.Location = new System.Drawing.Point(537, 11);
            this.dtpAdjQtyStartDate.Name = "dtpAdjQtyStartDate";
            this.dtpAdjQtyStartDate.Size = new System.Drawing.Size(104, 20);
            this.dtpAdjQtyStartDate.TabIndex = 13;
            // 
            // btnAdjQtyExit
            // 
            this.btnAdjQtyExit.Location = new System.Drawing.Point(1215, 7);
            this.btnAdjQtyExit.Name = "btnAdjQtyExit";
            this.btnAdjQtyExit.Size = new System.Drawing.Size(92, 23);
            this.btnAdjQtyExit.TabIndex = 12;
            this.btnAdjQtyExit.Text = "Exit";
            this.btnAdjQtyExit.UseVisualStyleBackColor = true;
            this.btnAdjQtyExit.Click += new System.EventHandler(this.btnAdjQtyExit_Click);
            // 
            // btnAdjQtyDisplay
            // 
            this.btnAdjQtyDisplay.Location = new System.Drawing.Point(1110, 7);
            this.btnAdjQtyDisplay.Name = "btnAdjQtyDisplay";
            this.btnAdjQtyDisplay.Size = new System.Drawing.Size(92, 23);
            this.btnAdjQtyDisplay.TabIndex = 11;
            this.btnAdjQtyDisplay.Text = "Display Report";
            this.btnAdjQtyDisplay.UseVisualStyleBackColor = true;
            this.btnAdjQtyDisplay.Click += new System.EventHandler(this.btnAdjQtyDisplay_Click);
            // 
            // crvAdjQtyAuditRpt
            // 
            this.crvAdjQtyAuditRpt.ActiveViewIndex = -1;
            this.crvAdjQtyAuditRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvAdjQtyAuditRpt.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvAdjQtyAuditRpt.Location = new System.Drawing.Point(6, 37);
            this.crvAdjQtyAuditRpt.Name = "crvAdjQtyAuditRpt";
            this.crvAdjQtyAuditRpt.Size = new System.Drawing.Size(1300, 880);
            this.crvAdjQtyAuditRpt.TabIndex = 6;
            this.crvAdjQtyAuditRpt.Visible = false;
            // 
            // tabPageNegQtyAudit
            // 
            this.tabPageNegQtyAudit.Controls.Add(this.btnNegQtyExit);
            this.tabPageNegQtyAudit.Controls.Add(this.btnNegQtyDisplay);
            this.tabPageNegQtyAudit.Controls.Add(this.crvNegQtyRpt);
            this.tabPageNegQtyAudit.Location = new System.Drawing.Point(4, 22);
            this.tabPageNegQtyAudit.Name = "tabPageNegQtyAudit";
            this.tabPageNegQtyAudit.Size = new System.Drawing.Size(1347, 949);
            this.tabPageNegQtyAudit.TabIndex = 7;
            this.tabPageNegQtyAudit.Text = "NegativeQtyAudit";
            this.tabPageNegQtyAudit.UseVisualStyleBackColor = true;
            // 
            // btnNegQtyExit
            // 
            this.btnNegQtyExit.Location = new System.Drawing.Point(1252, 8);
            this.btnNegQtyExit.Name = "btnNegQtyExit";
            this.btnNegQtyExit.Size = new System.Drawing.Size(92, 23);
            this.btnNegQtyExit.TabIndex = 14;
            this.btnNegQtyExit.Text = "Exit";
            this.btnNegQtyExit.UseVisualStyleBackColor = true;
            this.btnNegQtyExit.Click += new System.EventHandler(this.btnNegQtyExit_Click);
            // 
            // btnNegQtyDisplay
            // 
            this.btnNegQtyDisplay.Location = new System.Drawing.Point(1147, 8);
            this.btnNegQtyDisplay.Name = "btnNegQtyDisplay";
            this.btnNegQtyDisplay.Size = new System.Drawing.Size(92, 23);
            this.btnNegQtyDisplay.TabIndex = 13;
            this.btnNegQtyDisplay.Text = "Display Report";
            this.btnNegQtyDisplay.UseVisualStyleBackColor = true;
            this.btnNegQtyDisplay.Click += new System.EventHandler(this.btnNegQtyDisplay_Click);
            // 
            // crvNegQtyRpt
            // 
            this.crvNegQtyRpt.ActiveViewIndex = -1;
            this.crvNegQtyRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvNegQtyRpt.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvNegQtyRpt.Location = new System.Drawing.Point(6, 37);
            this.crvNegQtyRpt.Name = "crvNegQtyRpt";
            this.crvNegQtyRpt.Size = new System.Drawing.Size(1338, 908);
            this.crvNegQtyRpt.TabIndex = 4;
            this.crvNegQtyRpt.Visible = false;
            // 
            // tabPageReportBin
            // 
            this.tabPageReportBin.Controls.Add(this.cb_RB_L5REC);
            this.tabPageReportBin.Controls.Add(this.cb_RB_L4REC);
            this.tabPageReportBin.Controls.Add(this.cb_RB_LBIN1);
            this.tabPageReportBin.Controls.Add(this.cb_RB_L1REC);
            this.tabPageReportBin.Controls.Add(this.cb_RB_ELVMI);
            this.tabPageReportBin.Controls.Add(this.label18);
            this.tabPageReportBin.Controls.Add(this.cb_RB_LBIN2);
            this.tabPageReportBin.Controls.Add(this.cb_RB_L2MF);
            this.tabPageReportBin.Controls.Add(this.cb_RB_L2EL);
            this.tabPageReportBin.Controls.Add(this.cb_RB_L2AS);
            this.tabPageReportBin.Controls.Add(this.label17);
            this.tabPageReportBin.Controls.Add(this.crv_BinReport);
            this.tabPageReportBin.Controls.Add(this.btn_RB_Exit);
            this.tabPageReportBin.Controls.Add(this.btn_RB_DisplayRpt);
            this.tabPageReportBin.Location = new System.Drawing.Point(4, 22);
            this.tabPageReportBin.Name = "tabPageReportBin";
            this.tabPageReportBin.Size = new System.Drawing.Size(1347, 949);
            this.tabPageReportBin.TabIndex = 8;
            this.tabPageReportBin.Text = "Net/NonNet Bins";
            this.tabPageReportBin.UseVisualStyleBackColor = true;
            // 
            // cb_RB_L5REC
            // 
            this.cb_RB_L5REC.AutoSize = true;
            this.cb_RB_L5REC.Location = new System.Drawing.Point(345, 11);
            this.cb_RB_L5REC.Name = "cb_RB_L5REC";
            this.cb_RB_L5REC.Size = new System.Drawing.Size(60, 17);
            this.cb_RB_L5REC.TabIndex = 34;
            this.cb_RB_L5REC.Text = "L5REC";
            this.cb_RB_L5REC.UseVisualStyleBackColor = true;
            // 
            // cb_RB_L4REC
            // 
            this.cb_RB_L4REC.AutoSize = true;
            this.cb_RB_L4REC.Location = new System.Drawing.Point(279, 11);
            this.cb_RB_L4REC.Name = "cb_RB_L4REC";
            this.cb_RB_L4REC.Size = new System.Drawing.Size(60, 17);
            this.cb_RB_L4REC.TabIndex = 33;
            this.cb_RB_L4REC.Text = "L4REC";
            this.cb_RB_L4REC.UseVisualStyleBackColor = true;
            // 
            // cb_RB_LBIN1
            // 
            this.cb_RB_LBIN1.AutoSize = true;
            this.cb_RB_LBIN1.Location = new System.Drawing.Point(160, 11);
            this.cb_RB_LBIN1.Name = "cb_RB_LBIN1";
            this.cb_RB_LBIN1.Size = new System.Drawing.Size(56, 17);
            this.cb_RB_LBIN1.TabIndex = 32;
            this.cb_RB_LBIN1.Text = "LBIN1";
            this.cb_RB_LBIN1.UseVisualStyleBackColor = true;
            // 
            // cb_RB_L1REC
            // 
            this.cb_RB_L1REC.AutoSize = true;
            this.cb_RB_L1REC.Location = new System.Drawing.Point(97, 11);
            this.cb_RB_L1REC.Name = "cb_RB_L1REC";
            this.cb_RB_L1REC.Size = new System.Drawing.Size(60, 17);
            this.cb_RB_L1REC.TabIndex = 31;
            this.cb_RB_L1REC.Text = "L1REC";
            this.cb_RB_L1REC.UseVisualStyleBackColor = true;
            // 
            // cb_RB_ELVMI
            // 
            this.cb_RB_ELVMI.AutoSize = true;
            this.cb_RB_ELVMI.Location = new System.Drawing.Point(529, 11);
            this.cb_RB_ELVMI.Name = "cb_RB_ELVMI";
            this.cb_RB_ELVMI.Size = new System.Drawing.Size(58, 17);
            this.cb_RB_ELVMI.TabIndex = 30;
            this.cb_RB_ELVMI.Text = "ELVMI";
            this.cb_RB_ELVMI.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(430, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 13);
            this.label18.TabIndex = 29;
            this.label18.Text = "NonNettable Bins:";
            // 
            // cb_RB_LBIN2
            // 
            this.cb_RB_LBIN2.AutoSize = true;
            this.cb_RB_LBIN2.Location = new System.Drawing.Point(217, 11);
            this.cb_RB_LBIN2.Name = "cb_RB_LBIN2";
            this.cb_RB_LBIN2.Size = new System.Drawing.Size(56, 17);
            this.cb_RB_LBIN2.TabIndex = 28;
            this.cb_RB_LBIN2.Text = "LBIN2";
            this.cb_RB_LBIN2.UseVisualStyleBackColor = true;
            // 
            // cb_RB_L2MF
            // 
            this.cb_RB_L2MF.AutoSize = true;
            this.cb_RB_L2MF.Location = new System.Drawing.Point(708, 11);
            this.cb_RB_L2MF.Name = "cb_RB_L2MF";
            this.cb_RB_L2MF.Size = new System.Drawing.Size(53, 17);
            this.cb_RB_L2MF.TabIndex = 27;
            this.cb_RB_L2MF.Text = "L2MF";
            this.cb_RB_L2MF.UseVisualStyleBackColor = true;
            // 
            // cb_RB_L2EL
            // 
            this.cb_RB_L2EL.AutoSize = true;
            this.cb_RB_L2EL.Location = new System.Drawing.Point(650, 11);
            this.cb_RB_L2EL.Name = "cb_RB_L2EL";
            this.cb_RB_L2EL.Size = new System.Drawing.Size(51, 17);
            this.cb_RB_L2EL.TabIndex = 26;
            this.cb_RB_L2EL.Text = "L2EL";
            this.cb_RB_L2EL.UseVisualStyleBackColor = true;
            // 
            // cb_RB_L2AS
            // 
            this.cb_RB_L2AS.AutoSize = true;
            this.cb_RB_L2AS.Location = new System.Drawing.Point(592, 11);
            this.cb_RB_L2AS.Name = "cb_RB_L2AS";
            this.cb_RB_L2AS.Size = new System.Drawing.Size(52, 17);
            this.cb_RB_L2AS.TabIndex = 25;
            this.cb_RB_L2AS.Text = "L2AS";
            this.cb_RB_L2AS.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Nettable Bins:";
            // 
            // crv_BinReport
            // 
            this.crv_BinReport.ActiveViewIndex = -1;
            this.crv_BinReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crv_BinReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.crv_BinReport.Location = new System.Drawing.Point(6, 37);
            this.crv_BinReport.Name = "crv_BinReport";
            this.crv_BinReport.Size = new System.Drawing.Size(1079, 908);
            this.crv_BinReport.TabIndex = 17;
            this.crv_BinReport.Visible = false;
            // 
            // btn_RB_Exit
            // 
            this.btn_RB_Exit.Location = new System.Drawing.Point(994, 8);
            this.btn_RB_Exit.Name = "btn_RB_Exit";
            this.btn_RB_Exit.Size = new System.Drawing.Size(92, 23);
            this.btn_RB_Exit.TabIndex = 16;
            this.btn_RB_Exit.Text = "Exit";
            this.btn_RB_Exit.UseVisualStyleBackColor = true;
            this.btn_RB_Exit.Click += new System.EventHandler(this.btn_RB_Exit_Click);
            // 
            // btn_RB_DisplayRpt
            // 
            this.btn_RB_DisplayRpt.Location = new System.Drawing.Point(889, 8);
            this.btn_RB_DisplayRpt.Name = "btn_RB_DisplayRpt";
            this.btn_RB_DisplayRpt.Size = new System.Drawing.Size(92, 23);
            this.btn_RB_DisplayRpt.TabIndex = 15;
            this.btn_RB_DisplayRpt.Text = "Display Report";
            this.btn_RB_DisplayRpt.UseVisualStyleBackColor = true;
            this.btn_RB_DisplayRpt.Click += new System.EventHandler(this.btn_RB_DisplayRpt_Click);
            // 
            // frmAMM_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 962);
            this.Controls.Add(this.tabCntrlMain);
            this.Name = "frmAMM_Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AMM Reports";
            this.Load += new System.EventHandler(this.frmAMM_Report_Load);
            this.tabCntrlMain.ResumeLayout(false);
            this.tabPageElectron.ResumeLayout(false);
            this.tabPageElectron.PerformLayout();
            this.tabPageOAU_PurParts.ResumeLayout(false);
            this.tabPageIndPart.ResumeLayout(false);
            this.tabPageByUser.ResumeLayout(false);
            this.tabPageMetalTran.ResumeLayout(false);
            this.tabPagePullListAudit.ResumeLayout(false);
            this.tabPagePullListAudit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPickListMain)).EndInit();
            this.tabPageAdjQtyAudit.ResumeLayout(false);
            this.tabPageNegQtyAudit.ResumeLayout(false);
            this.tabPageReportBin.ResumeLayout(false);
            this.tabPageReportBin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCntrlMain;
        private System.Windows.Forms.TabPage tabPageElectron;
        private System.Windows.Forms.TabPage tabPageOAU_PurParts;
        private System.Windows.Forms.TabPage tabPageIndPart;
        private System.Windows.Forms.TabPage tabPageByUser;
        private System.Windows.Forms.Button btnEP_Exit;
        private System.Windows.Forms.Button btnEP_DisplayRpt;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvElectronRpt;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvOAUPartsRpt;
        private System.Windows.Forms.Button btnOAUPartsRptDisplay;
        private System.Windows.Forms.Button btnOAUPartsExit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpOAU_PartsEndDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpOAU_PartsBeginDate;
        private System.Windows.Forms.ComboBox cbPartNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpIndPartEndDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpIndPartBeginDate;
        private System.Windows.Forms.Button btnOAU_IndividualPartExit;
        private System.Windows.Forms.Button btnOAU_IndividualPartDisplay;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvOAU_IndPartTranRpt;
        private System.Windows.Forms.ComboBox cbUserID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpIndUserEndDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpIndUserBeginDate;
        private System.Windows.Forms.Button btnIndUserExit;
        private System.Windows.Forms.Button btnIndUserDisplayRpt;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvOAU_IndUserTranRpt;
        private System.Windows.Forms.TabPage tabPageMetalTran;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpOAU_MetalTranEndDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpOAU_MetalTranBeginDate;
        private System.Windows.Forms.Button btnOAU_MetalTranExit;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvOAU_MetalRpt;
        private System.Windows.Forms.Button btnOAU_MetalTranDisplay;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton rbElectronByPartNum;
        private System.Windows.Forms.RadioButton rbElectronByDate;
        private System.Windows.Forms.Label lbElectronEndDate;
        private System.Windows.Forms.DateTimePicker dtpElectronEndDate;
        private System.Windows.Forms.Label lbElectronBeginDate;
        private System.Windows.Forms.DateTimePicker dtpElectronBeginDate;
        private System.Windows.Forms.TabPage tabPagePullListAudit;
        private System.Windows.Forms.DataGridView dgvPickListMain;
        private System.Windows.Forms.Button btnPickListAuditDisplay;
        private System.Windows.Forms.RadioButton rbPickListAuditOpenJobs;
        private System.Windows.Forms.RadioButton rbPickListAuditClosedJobs;
        private System.Windows.Forms.RadioButton rbPickListAuditAllJobs;
        private System.Windows.Forms.Button btnPickListAuditExit;
        private System.Windows.Forms.DateTimePicker dtpPickListDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnPickListAuditReset;
        private System.Windows.Forms.TabPage tabPageAdjQtyAudit;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpAdjQtyEndDate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtpAdjQtyStartDate;
        private System.Windows.Forms.Button btnAdjQtyExit;
        private System.Windows.Forms.Button btnAdjQtyDisplay;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvAdjQtyAuditRpt;
        private System.Windows.Forms.TabPage tabPageNegQtyAudit;
        private System.Windows.Forms.Button btnNegQtyExit;
        private System.Windows.Forms.Button btnNegQtyDisplay;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvNegQtyRpt;
        private System.Windows.Forms.TabPage tabPageReportBin;
        private System.Windows.Forms.Label label17;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crv_BinReport;
        private System.Windows.Forms.Button btn_RB_Exit;
        private System.Windows.Forms.Button btn_RB_DisplayRpt;
        private System.Windows.Forms.CheckBox cb_RB_LBIN1;
        private System.Windows.Forms.CheckBox cb_RB_L1REC;
        private System.Windows.Forms.CheckBox cb_RB_ELVMI;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox cb_RB_LBIN2;
        private System.Windows.Forms.CheckBox cb_RB_L2MF;
        private System.Windows.Forms.CheckBox cb_RB_L2EL;
        private System.Windows.Forms.CheckBox cb_RB_L2AS;
        private System.Windows.Forms.CheckBox cb_RB_L4REC;
        private System.Windows.Forms.CheckBox cb_RB_L5REC;
    }
}

