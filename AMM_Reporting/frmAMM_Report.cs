﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;

namespace AMM_Reporting
{
    public partial class frmAMM_Report : Form
    {
        public frmAMM_Report()
        {
            InitializeComponent();
        }

        #region Events       
        private void frmAMM_Report_Load(object sender, EventArgs e)
        {
            //populateElectronReport();

            dtpElectronBeginDate.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dtpElectronEndDate.Value = DateTime.Now;           

            populatePickLickGrid();
        }

        private void btnEP_DisplayRpt_Click(object sender, EventArgs e)
        {
            populateElectronReport();
        }

        private void rbElectronByDate_CheckedChanged(object sender, EventArgs e)
        {
            lbElectronBeginDate.Visible = true;
            lbElectronEndDate.Visible = true;
            dtpElectronBeginDate.Visible = true;
            dtpElectronEndDate.Visible = true;
        }

        private void rbElectronByPartNum_CheckedChanged(object sender, EventArgs e)
        {
            lbElectronBeginDate.Visible = false;
            lbElectronEndDate.Visible = false;
            dtpElectronBeginDate.Visible = false;
            dtpElectronEndDate.Visible = false;
        }

        private void btnOAUPartsDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";

            ReportDocument cryRpt = new ReportDocument();   

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";           
            pdv1.Value = Convert.ToDateTime(this.dtpOAU_PartsBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";           
            pdv2.Value = Convert.ToDateTime(this.dtpOAU_PartsEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            crvOAUPartsRpt.ParameterFieldInfo = paramFields;

            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartTransAuditTest.rpt";
            //reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartTransAudit.rpt";
#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestPartTransAudit.rpt";             
#endif
            
            cryRpt.Load(reportString);            

            crvOAUPartsRpt.ReportSource = cryRpt;
            crvOAUPartsRpt.Visible = true;                         

            this.Refresh();            
        }


        private void btnOAU_IndividualPartDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";           

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";
            pdv1.Value = Convert.ToDateTime(this.dtpIndPartBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpIndPartEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@PartNum";
            pdv3.Value = this.cbPartNumber.Text;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            crvOAU_IndPartTranRpt.ParameterFieldInfo = paramFields;

            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdIndividualPartTransAudit.rpt";
#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestIndividualPartTransAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            crvOAU_IndPartTranRpt.ReportSource = cryRpt;
            crvOAU_IndPartTranRpt.Visible = true;

            this.Refresh();           
        }

        private void btnIndUserDisplayRpt_Click(object sender, EventArgs e)
        {
            string reportString = "";
            string userName = "";
            string entryPerson = "";
            string empID = "";
            string name;

            userName = this.cbUserID.SelectedValue.ToString();
            name = this.cbUserID.Text;

            if (userName.Contains("SHOP"))
            {
                entryPerson = "SHOP";
                empID = userName.Substring(5, (userName.Length - 5));
            }
            else
            {
                entryPerson = userName.Substring(0, userName.IndexOf("-"));
                empID = entryPerson;
            }
            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";
            pdv1.Value = Convert.ToDateTime(this.dtpIndUserBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpIndUserEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@UserID";
            pdv3.Value = empID;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@UserName";
            pdv4.Value = name;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            //ParameterField pf5 = new ParameterField();
            //ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            //pf5.Name = "@Name";
            //pdv5.Value = name;
            //pf5.CurrentValues.Add(pdv5);

            //paramFields.Add(pf5);

            crvOAU_IndUserTranRpt.ParameterFieldInfo = paramFields;

            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdIndividualUserTransAudit.rpt";

#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestIndividualUserTransAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            crvOAU_IndUserTranRpt.ReportSource = cryRpt;
            crvOAU_IndUserTranRpt.Visible = true;

            this.Refresh();           
        }

        private void btnEP_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOAUPartsExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOAU_IndividualPartExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnIndUserExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOAU_MetalTranExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabCntrlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabCntrlMain.SelectedIndex)
            {
                case 2:               
                    {
                        populatePartNumComboIndPartPage();
                        break;
                    }
                case 3:
                    {
                        populateUserIDComboIndUserPage();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        #endregion

        #region Other Methods

        private void populatePartNumComboIndPartPage()
        {
           
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetPurchasePartList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }

                this.cbPartNumber.DataSource = dt;
                this.cbPartNumber.DisplayMember = "PartNum";
                this.cbPartNumber.ValueMember = "PartNum";

            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        private void populateUserIDComboIndUserPage()
        {

            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetUserList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }

                this.cbUserID.DataSource = dt;
                this.cbUserID.DisplayMember = "UserName";
                this.cbUserID.ValueMember = "EntryPerson";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void populateElectronReport()
        {
            string reportString = "";

            ReportDocument cryRpt = new ReportDocument();
            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";
            pdv1.Value = Convert.ToDateTime(this.dtpElectronBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpElectronEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            //ParameterField pf3 = new ParameterField();
            //ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            //pf3.Name = "@PartNum";
            //pdv3.Value = this.cbPartNumber.Text;
            //pf3.CurrentValues.Add(pdv3);

            //paramFields.Add(pf3);

            


            if (rbElectronByDate.Checked == true)
            {
                reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdElectronAMMPartTransAuditByDate.rpt";
                crvElectronRpt.ParameterFieldInfo = paramFields;
#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAuditByDate.rpt";             
#endif

            }
            else
            {
                reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdElectronAMMPartTransAudit.rpt";
#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAudit.rpt";             
#endif
            }
            cryRpt.Load(reportString);

            crvElectronRpt.ReportSource = cryRpt;
            crvElectronRpt.Visible = true;

            this.Refresh();
            this.crvElectronRpt.Refresh();  

        }

        #endregion

        private void btnOAU_MetalTranDisplay_Click(object sender, EventArgs e)
        {

        }


        #region PickList Tab

        private void populatePickLickGrid()
        {
            int jobStatus = 2;
            string jobNumberStr = "";

            DataTable dt = new DataTable();

            if (this.rbPickListAuditOpenJobs.Checked == true)
            {
                jobStatus = 0;
            }
            else if (this.rbPickListAuditClosedJobs.Checked == true)
            {
                jobStatus = 1;
            }            

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_PickListMain", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNumSearchStr", jobNumberStr);
                        cmd.Parameters.AddWithValue("@JobStatus", jobStatus);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            dgvPickListMain.DataSource = dt;

        }        

        private void buttonMainExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }        

        private void rbPickListAuditOpenJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbPickListAuditOpenJobs.Checked == true)
            {
                populatePickLickGrid();
            }
        }

        private void rbPickListAuditClosedJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbPickListAuditClosedJobs.Checked == true)
            {
                populatePickLickGrid();
            }
        }

        private void rbPickListAuditAllJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbPickListAuditAllJobs.Checked == true)
            {
                populatePickLickGrid();
            }
        }

        private void btnPickListAuditReset_Click(object sender, EventArgs e)
        {
            dtpPickListDate.Value = DateTime.Today;
        }

        private void btnPickListAuditDisplay_Click(object sender, EventArgs e)
        {           
            displayPickPullAuditReport(dtpPickListDate.Value);                  
        }

        private void displayPickPullAuditReport(DateTime PullDate)
        {
            string reportString = "";
            string jobNumList = "";
            string jobNumStr = "";
            bool firstJob = true;

            DataTable dt = GetOAU_PickListJobNumberByPullDate(PullDate);
            
            foreach(DataRow row in dt.Rows)
            {
                jobNumStr = row["JobNum"].ToString();                   
              
                if (firstJob)
                {
                    jobNumList = jobNumStr;
                    firstJob = false;
                }
                else
                {
                    jobNumList += ", " + jobNumStr;
                }                  
            }

            ReportDocument cryRpt = new ReportDocument();
            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@PullDate";
            pdv1.Value = PullDate;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@JobNumList";
            pdv2.Value = jobNumList;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            frmReport frmRpt = new frmReport();

            frmRpt.crystalReportViewer1.ParameterFieldInfo = paramFields;                  

            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPickPullAudit3.rpt";           
#if DEBUG
            reportString = @"F:\Projects\AMM_Reporting\AMM_Reporting\reports\OAU_ProdPickPullAudit3.rpt";             
#endif
          
            cryRpt.Load(reportString);

            frmRpt.crystalReportViewer1.ReportSource = cryRpt;
            frmRpt.ShowDialog();
            frmRpt.crystalReportViewer1.Refresh();

        }

        private DataTable GetOAU_PickListJobNumberByPullDate(DateTime PullDate)
        {            
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_PickListJobNumberByPullDate", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PullDate", PullDate);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;

        }    

        #endregion       

        #region AdjQtyAuditReport

        private void btnAdjQtyExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdjQtyDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";            
           
            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@StartDate";
            pdv1.Value = Convert.ToDateTime(this.dtpAdjQtyStartDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpAdjQtyEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);            

            this.crvAdjQtyAuditRpt.ParameterFieldInfo = paramFields;

            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdAdjQtyAudit.rpt";

#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdAdjQtyAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            this.crvAdjQtyAuditRpt.ReportSource = cryRpt;
            this.crvAdjQtyAuditRpt.Visible = true;

            this.Refresh();           
        }
        #endregion

        #region NegativeQtyAuditReport

        private void btnNegQtyExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnNegQtyDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";

            ReportDocument cryRpt = new ReportDocument();          

            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinNegativeQtyAudit.rpt";

#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinNegativeQtyAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            this.crvNegQtyRpt.ReportSource = cryRpt;
            this.crvNegQtyRpt.Visible = true;

            this.Refresh();
        }
        #endregion


        #region NonNettable
        private void btn_RB_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        

        private void btn_RB_DisplayRpt_Click(object sender, EventArgs e)
        {
            string reportString = "";
            string binString = "";
            bool firstBin = true;

            if (cb_RB_ELVMI.Checked == true)
            {
                binString = "ELVMI";
                firstBin = false;
            }

            if (cb_RB_L2AS.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L2AS";
                firstBin = false;
            }

            if (cb_RB_L2EL.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L2EL";
                firstBin = false;
            }

            if (cb_RB_L2MF.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L2MF";
                firstBin = false;
            }

            if (cb_RB_L1REC.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L1REC";
                firstBin = false;
            }

            if (cb_RB_LBIN2.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "LBIN2";
                firstBin = false;
            }

            if (cb_RB_LBIN1.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "LBIN1";
                firstBin = false;
            }

            if (cb_RB_L4REC.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L4REC";
                firstBin = false;
            }

            if (cb_RB_L5REC.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L5REC";
                firstBin = false;
            }

            ReportDocument cryRpt = new ReportDocument();
            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BinStr";
            pdv1.Value = binString;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            //ParameterField pf2 = new ParameterField();
            //ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            //pf2.Name = "@EndDate";
            //pdv2.Value = Convert.ToDateTime(this.dtpElectronEndDate.Text);
            //pf2.CurrentValues.Add(pdv2);

            //paramFields.Add(pf2);          

            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinAuditReport.rpt";
            crv_BinReport.ParameterFieldInfo = paramFields;
#if DEBUG
            reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinAuditReport.rpt";
            //reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAuditByDate.rpt";             
#endif

            cryRpt.Load(reportString);

            crv_BinReport.ReportSource = cryRpt;
            crv_BinReport.Visible = true;

            this.Refresh();
            this.crv_BinReport.Refresh();  
        }
        #endregion       
       

    }
}
